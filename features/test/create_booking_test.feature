@createBooking
Feature: I can create a booking

  Scenario: Successfully create booking
    Given the admin is authorized
    When the admin hit create-booking API endpoint
    Then show success create-booking response